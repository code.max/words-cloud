var canva = document.getElementById( 'word-cloud' );
var config = 	
{ 
	list: words,
	gridSize: Math.round( 16 * canva.offsetWidth / 1024 ),
	weightFactor: function ( size ) 
	{
		let factor = factors.desktop[ size ];
		if( document.documentElement.clientWidth < breaks.tablet )
		{
			factor = factors.mobile[ size ];
		}
		if( document.documentElement.clientWidth < breaks.desktop )
		{
			factor = factors.tablet[ size ];
		}
		return size * factor;
	},
	fontFamily: 'Libre Baskerville, Sans-serif',
	color: function ( word, weight ) 
	{
	return colors[ weight ];
	},
	rotateRatio: 0.5,
	rotationSteps: 10,
	shape: 'circle',
};

WordCloud( canva, config );
